import React from "react";
import { Card } from "semantic-ui-react";
import factory from "../ethereum/factory";

class CampaignIndex extends React.Component {
    // state = {
    //     campaigns: []
    // }
    // async componentDidMount() {
    //     const campaigns = await factory.methods.getDeployedCampaigns().call();
    //     console.log(campaigns);
    //     this.setState({
    //         campaigns: campaigns || []
    //     })
    // }

    static async getInitialProps() {
        const campaigns = await factory.methods.getDeployedCampaigns().call();
        return {
            campaigns: campaigns || []
        }
    }

    renderCampaigns() {
        const { campaigns } = this.props;
        const items = campaigns.map(address => {
            return {
                header: address,
                description: <a>View Campaign </a>,
                fluid: true
            };
        });
        return <Card.Group items = {items} />
    }


    render() {
        // const { campaigns } = this.state;
        const { campaigns } = this.props;
        return (
            <div>
                <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css" />
                {/* it is root page 
                {
                    campaigns.map(c => {
                        return (
                            <div key={c}>{c}</div>
                        )
                    })
                } */}
                {this.renderCampaigns()}
            </div>
        )
    }
}

export default CampaignIndex;