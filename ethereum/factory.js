import web3 from "./web3";
import CampaignFactory from "./build/CampaignFactory";

const instance = new web3.eth.Contract(
    JSON.parse(CampaignFactory.interface),
    '0x465eBf5f31b97A390414B7A62a4969a10a830f8a'
)

export default instance;